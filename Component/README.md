# Component

[![CI Status](https://img.shields.io/travis/tothemoon/Component.svg?style=flat)](https://travis-ci.org/tothemoon/Component)
[![Version](https://img.shields.io/cocoapods/v/Component.svg?style=flat)](https://cocoapods.org/pods/Component)
[![License](https://img.shields.io/cocoapods/l/Component.svg?style=flat)](https://cocoapods.org/pods/Component)
[![Platform](https://img.shields.io/cocoapods/p/Component.svg?style=flat)](https://cocoapods.org/pods/Component)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Component is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Component'
```

## Author

tothemoon, 243912452@qq.com

## License

Component is available under the MIT license. See the LICENSE file for more info.
