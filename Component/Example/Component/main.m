//
//  main.m
//  Component
//
//  Created by tothemoon on 03/29/2022.
//  Copyright (c) 2022 tothemoon. All rights reserved.
//

@import UIKit;
#import "CTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CTAppDelegate class]));
    }
}
