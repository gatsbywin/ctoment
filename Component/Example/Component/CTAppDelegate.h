//
//  CTAppDelegate.h
//  Component
//
//  Created by tothemoon on 03/29/2022.
//  Copyright (c) 2022 tothemoon. All rights reserved.
//

@import UIKit;

@interface CTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
